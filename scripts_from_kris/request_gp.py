import cdsapi

area=[81, 10, 76, 30] # Modify bounding box (N,W,S,E))

yearis='2019' # 1979 2021 last is excluded
months=['01']
days=['01',]
times=['00:00',]                        
varsare=['geopotential']            
fname='gp_surf.nc'

c = cdsapi.Client()
c.retrieve(
    	'reanalysis-era5-single-levels',
    	{
    		'product_type': 'reanalysis',
    		'format': 'netcdf',
       		'variable': varsare,
        	'year': yearis,
        	'month': months,
        	'day': days,
        	'time': times,
        	'area': area,
    	},
    	fname)
