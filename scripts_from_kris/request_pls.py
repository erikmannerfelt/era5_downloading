import cdsapi

area=[81, 10, 76, 30] # Modify bounding box (N,W,S,E)
# Target location is 66.9373, -53.6194
years=list(range(1940,2023)) 


Nm=12 # Number of months
plev=1000 # One level at a time for now
# plevs= 1000:-25:750, 700:-50:300
# p=p0*exp(-z/(RT/g)) R=287, g=9.81, p0=1e3
# For Svalbard (z<2000 m) I think that all pressure levels from 700 to 1000 would be fine
# So that's plev=1000,975,950,925,900,875,850,825,800,775,750,700
plevis=['%d' %(plev)]
days=['01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
	 '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
	  '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31',]
times=['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00',
            '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00', '21:00', '22:00', '23:00',]
varsare=['geopotential', 'temperature']

c = cdsapi.Client()
for k in range(len(years)):
	for j in range(Nm):
		months='%d' % (j+1)
		yearis='%d' % (years[k])
		fname='plev_m%d_p%d_%s.nc' % (j+1,plev,yearis)
		print(fname)
		
			
		c.retrieve(
	    		'reanalysis-era5-pressure-levels',
	    		{
	    	    		'product_type': 'reanalysis',
	    	    		'format': 'netcdf',
	    	    		'variable': varsare,
	        		'pressure_level': plevis,
	        		'year': yearis,
	        		'month': months,
	        		'day': days,
	        		'time': times,
	        		'area': area,
	    		},
	    		fname)




