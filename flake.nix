{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        python_packages = pkgs.python310Packages.override {
          overrides = self: super: {
            cdsapi = super.buildPythonPackage rec {
              pname = "cdsapi";
              version = "0.6.1";
              src = super.fetchPypi {
                inherit pname version;
                sha256 = "fUDFjj/T51qKzc3IHqtO+bb3Y7KQK6AdfRc482UqWjA=";
              };
              propagatedBuildInputs = with super; [
                requests
                tqdm
              ];
              setuptoolsCheckPhase = "true";
            };
          };
        };
        
        python_from_requirements = requirements_path: (
          let
            raw_requirements = (pkgs.lib.splitString "\n" (pkgs.lib.readFile requirements_path));
            requirements = builtins.map (s: builtins.replaceStrings [ " " ] [ "" ] s) (builtins.filter (s: (builtins.stringLength s) > 1) raw_requirements);
          in
          python_packages.python.withPackages (_: map (req: (builtins.getAttr req python_packages)) requirements)
        );

        python = python_from_requirements ./requirements.txt;

        packages = pkgs.lib.attrsets.recursiveUpdate
          (builtins.listToAttrs (map (pkg: { name = pkg.pname; value = pkg; }) (with pkgs; [
            pre-commit
            zsh
          ])))
          {
            inherit python;
          };

      in
      {
        inherit packages;
        defaultPackage = packages.python;

        devShell = pkgs.mkShell {
          name = "ERA5";
          buildInputs = pkgs.lib.attrValues packages;
          shellHook = ''

          '';
        };
      }

    );
}
