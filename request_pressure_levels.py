from __future__ import annotations
import cdsapi
import numpy as np
import datetime
from pathlib import Path
from tqdm.contrib.concurrent import thread_map

class Bounds:
    def __init__(self, left: float, bottom: float, right: float, top: float):
        self.left = left
        self.bottom = bottom
        self.right = right
        self.top = top

    def to_era5(self) -> list[float]:
        return [self.top, self.left, self.bottom, self.right]


def get_pressure_level(elevation: float, temperature: float = 273.0) -> int:
    raw_pressure_level = 1e3 * np.exp(-elevation / ((287 * temperature) / 9.81))

    return raw_pressure_level

    era5_levels = np.arange(0, 1025, step=25)
    return era5_levels[np.argmin(np.abs(era5_levels - raw_pressure_level))]


def get_era5_levels() -> np.ndarray:
    return np.sort(
        np.concatenate(
            (
                [1, 2, 3, 5, 7, 10, 20, 30, 50, 70],
                np.arange(100, 250, 25),
                np.arange(250, 750, 50),
                np.arange(750, 1025, 25),
            )
        )
    )


def get_pressure_level_range(
    min_elevation: float, max_elevation: float, min_temperature: float = 223.0, max_temperature: float = 323.0
):
    levels = []
    for temp in [min_temperature, max_temperature]:
        for elev in [min_elevation, max_elevation]:
            levels.append(get_pressure_level(elev, temp))
    era5_levels = get_era5_levels()

    level_indices = np.digitize(levels, era5_levels) - 1

    return era5_levels[np.arange(np.min(level_indices), np.max(level_indices) + 1)]


def generate_queries(
    bounds: Bounds,
    min_year: int,
    max_year: int,
    elev_range: tuple[float, float],
    temp_range: tuple[float, float] | None = None,
    variables: list[str] = ["temperature", "geopotential"],
    ) -> list[tuple[str, dict[str, object], str]]:
    times = [f"{str(hour).zfill(2)}:00" for hour in range(24)]
    if temp_range is not None:
        pressure_levels = get_pressure_level_range(*elev_range, *temp_range)
    else:
        pressure_levels = get_pressure_level_range(*elev_range)

    era5_bounds = bounds.to_era5()
    queries = []

    for year in reversed(range(min_year, max_year + 1)):
        for month in range(1, 13):
            for pressure_level in pressure_levels:
                last_day = (
                    datetime.datetime(year + (1 if month == 12 else 0), (1 if month == 12 else month + 1), 1)
                    - datetime.timedelta(hours=1)
                ).date()

                days = list(range(1, last_day.day + 1))

                output_filename = Path(f"raw/plev_{month}_{pressure_level}_{year}.nc")

                output_filename.parent.mkdir(exist_ok=True, parents=True)

                if output_filename.is_file():
                    continue

                queries.append(
                    (
                        "reanalysis-era5-pressure-levels",
                        {
                            "product_type": "reanalysis",
                            "format": "netcdf",
                            "variable": variables,
                            "pressure_level": float(pressure_level),
                            "year": year,
                            "month": month,
                            "day": days,
                            "time": times,
                            "area": era5_bounds,
                        },
                        str(output_filename),
                    )
                )
    return queries


def download_data(
    bounds: Bounds,
    min_year: int,
    max_year: int,
    elev_range: tuple[float, float],
    temp_range: tuple[float, float] | None = None,
    variables: list[str] = ["temperature", "geopotential"],
):
    queries = generate_queries(bounds=bounds, min_year=min_year, max_year=max_year, elev_range=elev_range, temp_range=temp_range, variables=variables)

    client = cdsapi.Client(quiet=True)

    thread_map(lambda args: client.retrieve(*args), queries, max_workers=8, desc="Downloading data", smoothing=0.1) 


def download_svalbard():
    bounds = Bounds(left=10, bottom=76, right=30, top=81)
    min_year = 1940
    max_year = 2023
    elev_range = (0.0, 2000.0)

    download_data(bounds, min_year, max_year, elev_range)


if __name__ == "__main__":
    download_svalbard()
